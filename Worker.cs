using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.Configuration;
using RushJsUpdateBot.Services;

namespace RushJsUpdateBot
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> logger;
        private readonly VersionControlOptions versionControlOptions;
        private readonly IVersionControlService versionControlService;
        private readonly ProjectUpdateService projectUpdateService;

        public Worker(ILogger<Worker> logger, IOptions<VersionControlOptions> versionControlOptions, IVersionControlService versionControlService, ProjectUpdateService projectUpdateService)
        {
            this.logger = logger;
            this.versionControlOptions = versionControlOptions.Value;
            this.versionControlService = versionControlService;
            this.projectUpdateService = projectUpdateService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                this.logger.LogInformation("Cloning {RepositoriesCount} repositories",
                    this.versionControlOptions.Repositories.Count);

                foreach (var repository in this.versionControlOptions.Repositories)
                {
                    await this.versionControlService.CloneRepository(repository.Name, stoppingToken);
                    var path = this.versionControlOptions.FormatDirectory(repository.Name);
                    await this.versionControlService.ConfigureIdentity(
                        path,
                        this.versionControlOptions.User.Name,
                        this.versionControlOptions.User.Email,
                        stoppingToken
                    );
                    await this.projectUpdateService.ProcessProject(new UpdateProject(repository, path), stoppingToken);
                }
                
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Environment.Exit(2);
            }
        }
    }
}
