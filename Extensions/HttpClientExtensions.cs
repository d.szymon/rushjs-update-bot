﻿using System.Collections.Generic;
using System.Net.Http;

namespace RushJsUpdateBot.Extensions
{
    public static class HttpClientExtensions
    {
        public static void AddDefaultRequestHeaders(this HttpClient client, Dictionary<string, string> headers)
        {
            foreach (var (name, value) in headers)
            {
                client.DefaultRequestHeaders.Add(name, value);
            }
        }

        public static HttpClient CreateWithDefaultRequestHeaders(this IHttpClientFactory factory,
            Dictionary<string, string> headers)
        {
            var client = factory.CreateClient();
            client.AddDefaultRequestHeaders(headers);
            return client;
        }
    }
}