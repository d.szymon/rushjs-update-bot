using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models.Configuration;
using RushJsUpdateBot.Services;

namespace RushJsUpdateBot
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.AddSingleton<RushConfigurationFactory>();
                    services.Configure<VersionControlOptions>(hostContext.Configuration.GetSection("VersionControl"));
                    services.AddSingleton<IVersionControlService, GitService>();
                    services.AddSingleton<ProjectUpdateService>();
                    services.Configure<PackagesOptions>(hostContext.Configuration.GetSection("Packages"));
                    services.AddSingleton<PackageJsonFactory>();
                    services.AddSingleton<INodeRegistryService, NpmRegistryService>();
                    services.Configure<NpmRegistryOptions>(hostContext.Configuration.GetSection("NpmRegistry"));
                    services.AddHttpClient();
                    services.AddSingleton<PackageUpdateService>();
                    services.AddSingleton<PackageUpdateGroupFactory>();
                    services.AddSingleton<RushJsService>();
                    services.AddSingleton<IVersionControlApiService, GitlabApiService>();
                });
    }
}
