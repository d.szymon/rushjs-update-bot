FROM mcr.microsoft.com/dotnet/runtime:5.0-alpine AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine AS build
WORKDIR /src
COPY ["RushJsUpdateBot.csproj", "."]
RUN dotnet restore "./RushJsUpdateBot.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "RushJsUpdateBot.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "RushJsUpdateBot.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app

RUN apk add --update nodejs nodejs-npm git openssh-client

ARG SSH_KEY
ENV SSH_KEY $SSH_KEY

RUN mkdir /root/.ssh/
RUN echo "$SSH_KEY" > /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts
RUN ssh-keyscan github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "RushJsUpdateBot.dll"]