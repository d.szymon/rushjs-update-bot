﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.Configuration;

namespace RushJsUpdateBot.Services
{
    public class ProjectUpdateService
    {
        private readonly ILogger<ProjectUpdateService> logger;
        private readonly RushConfigurationFactory rushConfigurationFactory;
        private readonly PackageJsonFactory packageJsonFactory;
        private readonly PackageUpdateService packageUpdateService;
        private readonly PackageUpdateGroupFactory packageUpdateGroupFactory;
        private readonly PackagesOptions packagesOptions;

        public ProjectUpdateService(RushConfigurationFactory rushConfigurationFactory,
            ILogger<ProjectUpdateService> logger,
            PackageJsonFactory packageJsonFactory,
            PackageUpdateService packageUpdateService, 
            PackageUpdateGroupFactory packageUpdateGroupFactory,
            IOptions<PackagesOptions> packagesOptions
        )
        {
            this.rushConfigurationFactory = rushConfigurationFactory;
            this.logger = logger;
            this.packageJsonFactory = packageJsonFactory;
            this.packageUpdateService = packageUpdateService;
            this.packageUpdateGroupFactory = packageUpdateGroupFactory;
            this.packagesOptions = packagesOptions.Value;
        }

        public async Task ProcessProject(UpdateProject project, CancellationToken cancellationToken = default)
        {
            this.logger.LogInformation("Processing {Path}", project.Path);
            var rushConfiguration = await this.rushConfigurationFactory.FromFile(Path.Join(project.Path, "rush.json"), cancellationToken);
            
            this.logger.LogTrace("Found {ProjectsCount} in {Path}", rushConfiguration.Projects.Count, project.Path);
            var deps = await BuildDependencyList(rushConfiguration, cancellationToken);
            this.logger.LogInformation("Collected {DepsCount} dependencies from {ProjectsCount} projects", deps.Count, rushConfiguration.Projects.Count);

            var updateGroups = await this.packageUpdateGroupFactory.BuildUpdateGroups(deps, cancellationToken);

            foreach (var updateGroup in updateGroups)
            {
                await this.packageUpdateService.ProcessGroup(project, rushConfiguration, updateGroup, cancellationToken);
               
                if (this.packagesOptions.SleepTime > 0)
                {
                    this.logger.LogInformation("Waiting for {SleepTime} seconds", this.packagesOptions.SleepTime);
                    await Task.Delay(TimeSpan.FromSeconds(this.packagesOptions.SleepTime), cancellationToken);
                }
            }
        }

        protected async Task<List<NodeDependency>> BuildDependencyList(RushConfiguration rushConfiguration, CancellationToken cancellationToken = default)
        {
            var deps = new List<NodeDependency>();

            foreach (var project in rushConfiguration.Projects)
            {
                var pjson = await this.packageJsonFactory.FromFile(project.PackageJsonPath, cancellationToken);

                if (pjson.Dependencies == null || pjson.Dependencies.Count == 0)
                {
                    continue;
                }

                foreach (var dependency in deps.Where(d => !d.Projects.Contains(project.Name)))
                {
                    if (pjson.Dependencies.ContainsKey(dependency.Name))
                    {
                        dependency.Projects.Add(project.Name);
                    }
                }

                deps.AddRange(
                    pjson.Dependencies
                        .Where(dependency => NodeDependency.IsSupported(dependency.Value))
                        .Where(dependency => !deps.Any(d => d.Name.Equals(dependency.Key)))
                        .Select(dependency => new NodeDependency(new []{ project.Name }, dependency))
                );
            }

            return deps;
        }
    }
}