﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.Configuration;

namespace RushJsUpdateBot.Services
{
    public class PackageUpdateGroupFactory
    {
        private readonly INodeRegistryService nodeRegistryService;
        private readonly PackagesOptions packagesOptions;

        public PackageUpdateGroupFactory(INodeRegistryService nodeRegistryService, IOptions<PackagesOptions> packagesOptions)
        {
            this.nodeRegistryService = nodeRegistryService;
            this.packagesOptions = packagesOptions.Value;
        }

        public async Task<List<PackageUpdateGroup>> BuildUpdateGroups(IEnumerable<NodeDependency> deps, CancellationToken cancellationToken)
        {
            var groups = new List<PackageUpdateGroup>();

            foreach (var dep in deps)
            {
                var minor = await this.nodeRegistryService.GetNewestMinor(dep, cancellationToken);
                var latest = await this.nodeRegistryService.GetNewestVersion(dep, cancellationToken);
                var update = new PackageUpdate(dep, minor, latest);

                if (!update.HasMajorUpdate && !update.HasMinorUpdate)
                {
                    continue;
                }

                var requestedGroup = this.packagesOptions.Groups.FirstOrDefault(g => g.Packages.Contains(dep.Name));

                if (requestedGroup == null)
                {
                    groups.Add(new PackageUpdateGroup(update));
                    continue;
                }

                var existingGroup = groups.FirstOrDefault(g => g.Name.Equals(requestedGroup.Name));

                if (existingGroup == null)
                {
                    groups.Add(new PackageUpdateGroup(requestedGroup.Name, update));
                }
                else
                {
                    existingGroup.Updates.Add(update);
                }
            }

            return groups;
        }
    }
}