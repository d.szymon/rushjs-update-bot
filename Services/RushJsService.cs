﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.Configuration;
using RushJsUpdateBot.Models.RushJs;

namespace RushJsUpdateBot.Services
{
    public class RushJsService
    {
        private readonly string versionControlEmail;

        public RushJsService(IOptions<VersionControlOptions> versionControlOptions)
        {
            this.versionControlEmail = versionControlOptions.Value.User.Email;
        }

        public Task UpdatePackage(RushConfiguration configuration, NodeDependency dependency, NodeDependencyVersion targetVersion, CancellationToken cancellationToken)
        {
            var projectPath = configuration.GetProjectPath(dependency.Projects.First());
            var p = Process.Start(new ProcessStartInfo("node")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                ArgumentList =
                    {
                    Path.GetRelativePath(projectPath,
                        Path.Join(configuration.RootDirectory,
                            "common/scripts/install-run-rush.js")),
                    "add",
                    "-p",
                    $"{dependency.Name}@{targetVersion}",
                    "-m"
                },
                WorkingDirectory = projectPath,
            });

            cancellationToken.Register(() => p?.Kill());

            if (p == null)
            {
                throw new SystemException("Failed to start Rush.js");
            }

            return p.WaitForExitAsync(cancellationToken);
        }

        public async Task AddOrUpdateChangelog(RushConfiguration configuration, NodeDependency dependency,
            NodeDependencyVersion targetVersion, CancellationToken cancellationToken)
        {
            var isMajor = dependency.Version.Major != targetVersion.Major;
            var fileSuffix = isMajor ? "-major" : string.Empty;
            
            foreach (var project in dependency.Projects)
            {
                var changelogPath = Path.Join(
                    configuration.ChangelogDirectory,
                    project.Replace('/', Path.DirectorySeparatorChar),
                    $"rushjs-bot-{dependency.Name}{fileSuffix}.json"
                );
                var changelog = new RushChangelog(project, this.versionControlEmail, new[]
                {
                    new RushChangelogChange(project, $"Update {dependency.Name} to {targetVersion}"),
                });

                var changelogDirectory = Path.GetDirectoryName(changelogPath);

                if (!Directory.Exists(changelogDirectory))
                {
                    Directory.CreateDirectory(changelogDirectory ?? throw new InvalidOperationException());
                }

                await using var stream = File.OpenWrite(changelogPath);
                await JsonSerializer.SerializeAsync(stream, changelog, new JsonSerializerOptions()
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                }, cancellationToken);
            }
        }

        public Task Purge(RushConfiguration configuration, CancellationToken cancellationToken)
        {
            var projectPath = configuration.RootDirectory;
            var p = Process.Start(new ProcessStartInfo("node")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                ArgumentList =
                {
                    Path.GetRelativePath(projectPath,
                        Path.Join(configuration.RootDirectory,
                            "common/scripts/install-run-rush.js")),
                    "purge"
                },
                WorkingDirectory = projectPath,
            });

            cancellationToken.Register(() => p?.Kill());

            if (p == null)
            {
                throw new SystemException("Failed to start Rush.js");
            }

            return p.WaitForExitAsync(cancellationToken);
        }
    }
}