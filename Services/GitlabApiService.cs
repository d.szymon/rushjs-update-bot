﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Extensions;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models.Configuration;
using RushJsUpdateBot.Models.Http;

namespace RushJsUpdateBot.Services
{
    public class GitlabApiService : IVersionControlApiService
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly VersionControlOptions versionControlOptions;
        private string BaseApiUrl => this.versionControlOptions.Api.Url;
        private readonly Dictionary<string, string> defaultRequestHeaders;

        public GitlabApiService(IHttpClientFactory httpClientFactory, IOptions<VersionControlOptions> versionControlOptions)
        {
            this.httpClientFactory = httpClientFactory;
            this.versionControlOptions = versionControlOptions.Value;
            this.defaultRequestHeaders = new Dictionary<string, string>()
            {
                {"PRIVATE-TOKEN", this.versionControlOptions.Api.AccessToken},
            };
        }

        public async Task CreateMergeRequest(string projectId, string branch, CancellationToken cancellationToken)
        {
            var mergeRequestId = await GetExistingMergeRequest(projectId, branch, cancellationToken);

            if (mergeRequestId != null)
            {
                return;
            }

            await OpenMergeRequest(projectId, branch, cancellationToken);
        }

        protected async Task OpenMergeRequest(string projectId, string branch, CancellationToken cancellationToken)
        {
            var client = this.httpClientFactory.CreateWithDefaultRequestHeaders(this.defaultRequestHeaders);

            var response = await client.PostAsJsonAsync($"{this.BaseApiUrl}/api/v4/projects/{projectId}/merge_requests",
                new GitlabApiCreateMergeRequestRequest()
                {
                    Id = projectId,
                    SourceBranch = branch,
                    TargetBranch = this.versionControlOptions.DefaultBranch,
                    Title = branch,
                }, cancellationToken);

            response.EnsureSuccessStatusCode();
        }

        protected async Task<ulong?> GetExistingMergeRequest(string projectId, string branch, CancellationToken cancellationToken)
        {
            var client = this.httpClientFactory.CreateWithDefaultRequestHeaders(this.defaultRequestHeaders);

            var response = await client.GetFromJsonAsync<List<GitlabApiMergeRequestsListItemResponse>>(
                $"{this.BaseApiUrl}/api/v4/projects/{projectId}/merge_requests?state=opened&source_branch={Uri.EscapeUriString(branch)}",
                new JsonSerializerOptions()
                {
                    PropertyNameCaseInsensitive = true,
                },
                cancellationToken
            );
            
            if (response == null || !response.Any())
            {
                return null;
            }

            return response.First().Id;
        }
    }
}