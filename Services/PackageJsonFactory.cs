﻿using System;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using RushJsUpdateBot.Models;

namespace RushJsUpdateBot.Services
{
    public class PackageJsonFactory
    {
        public async Task<PackageJson> FromFile(string path, CancellationToken cancellationToken = default)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"Missing package.json file in \"{path}\"", path);
            }
            
            await using var stream = File.OpenRead(path);
            return await JsonSerializer.DeserializeAsync<PackageJson>(stream, new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
            }, cancellationToken);
        }
    }
}