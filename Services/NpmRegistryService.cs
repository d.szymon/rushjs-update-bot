﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.Configuration;
using RushJsUpdateBot.Models.Http;

namespace RushJsUpdateBot.Services
{
    public class NpmRegistryService : INodeRegistryService
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ConcurrentDictionary<string, NpmRegistryPackageResponse> packageCache = new();
        private readonly NpmRegistryOptions npmRegistryOptions;

        public NpmRegistryService(IHttpClientFactory httpClientFactory, IOptions<NpmRegistryOptions> npmRegistryOptions)
        {
            this.httpClientFactory = httpClientFactory;
            this.npmRegistryOptions = npmRegistryOptions.Value;
        }

        public async Task<NodeDependencyVersion> GetNewestMinor(NodeDependency package, CancellationToken cancellationToken)
        {
            var packageData = await GetPackageMetadata(package.Name, cancellationToken);
            return NodeDependencyVersion.GetNewestVersion(packageData.Versions.Keys
                .ToList()
                .Select(key => new NodeDependencyVersion(key))
                .Where(version => version.Major == package.Version.Major));
        }

        public async Task<NodeDependencyVersion> GetNewestVersion(NodeDependency package, CancellationToken cancellationToken)
        {
            var packageData = await GetPackageMetadata(package.Name, cancellationToken);
            return new NodeDependencyVersion(packageData.DistTags["latest"]);
        }

        protected async Task<NpmRegistryPackageResponse> GetPackageMetadata(string packageName, CancellationToken cancellationToken)
        {
            if (this.packageCache.TryGetValue(packageName, out var packageData))
            {
                return packageData;
            }

            var httpClient = this.httpClientFactory.CreateClient();
            packageData = await httpClient.GetFromJsonAsync<NpmRegistryPackageResponse>(this.npmRegistryOptions.GetPackageUrl(packageName), new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
            }, cancellationToken);

            this.packageCache.TryAdd(packageName, packageData);
            return packageData;
        }
    }
}