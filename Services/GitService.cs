﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models.Configuration;

namespace RushJsUpdateBot.Services
{
    public class GitService : IVersionControlService
    {
        private readonly VersionControlOptions versionControlOptionsOptions;
        private readonly ILogger<GitService> logger;

        public GitService(IOptions<VersionControlOptions> versionControlOptions, ILogger<GitService> logger)
        {
            this.logger = logger;
            this.versionControlOptionsOptions = versionControlOptions.Value;
        }

        public async Task ConfigureIdentity(string path, string name, string email, CancellationToken cancellationToken)
        {
            await ExecuteGitCommand(path, cancellationToken, "config", "--global", "user.name", $"\"{name.Replace("\"", "\\\"")}\"");
            await ExecuteGitCommand(path, cancellationToken, "config", "--global", "user.email", $"\"{email}\"");
        }

        public async Task CloneRepository(string project, CancellationToken cancellationToken)
        {
            var repositoryUrl = this.versionControlOptionsOptions.FormatCloneUrl(project);
            var path = this.versionControlOptionsOptions.FormatDirectory(project);

            if (Directory.Exists(path))
            {
                await Task.Factory.StartNew(() => Directory.Delete(path, true), cancellationToken);
            }
            
            this.logger.LogTrace("Cloning from {RepositoryUrl} to {Path}", repositoryUrl, path);

            await ExecuteGitCommand(Environment.CurrentDirectory, cancellationToken, "clone", repositoryUrl, path);
            
            this.logger.LogTrace("Cloned from {RepositoryUrl} successfully", repositoryUrl);
        }

        public async Task CreateBranchFromDefault(string path, string branchName, CancellationToken cancellationToken)
        {
            await ExecuteGitCommand(path, cancellationToken,  "checkout", this.versionControlOptionsOptions.DefaultBranch);
            await ExecuteGitCommand(path, cancellationToken, "checkout", branchName);
            await ExecuteGitCommand(path, cancellationToken, "checkout", "-b", branchName);
            await ExecuteGitCommand(path, cancellationToken, "pull", "origin", this.versionControlOptionsOptions.DefaultBranch, "-X",
                "theirs");
        }

        public async Task PushChanges(string path, string message, CancellationToken cancellationToken)
        {
            await ExecuteGitCommand(path, cancellationToken, "add", ".");
            await ExecuteGitCommand(path, cancellationToken, "commit", "-m", "\"", message.Replace("\"", "\\\""), "\"");
            await ExecuteGitCommand(path, cancellationToken, "push", "origin", "HEAD");
        }

        protected Task ExecuteGitCommand(string cwd, CancellationToken cancellationToken, params string[] arguments)
        {
            var p = Process.Start(new ProcessStartInfo("git", string.Join(' ', arguments))
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                WorkingDirectory = cwd,
            });

            cancellationToken.Register(() => p?.Kill());

            if (p == null)
            {
                throw new SystemException("Failed to start git process");
            }
            
            return p.WaitForExitAsync(cancellationToken);
        }
    }
}