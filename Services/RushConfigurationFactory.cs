﻿using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.RushJs;

namespace RushJsUpdateBot.Services
{
    public class RushConfigurationFactory
    {
        private readonly ILogger<RushConfigurationFactory> logger;

        public RushConfigurationFactory(ILogger<RushConfigurationFactory> logger)
        {
            this.logger = logger;
        }

        public async Task<RushConfiguration> FromFile(string path, CancellationToken cancellationToken = default)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Missing Rush.js configuration file", path);
            }

            this.logger.LogTrace("Reading configuration from {Path}", path);
            await using var stream = File.OpenRead(path);
            var rushJson = await JsonSerializer.DeserializeAsync<RushJson>(stream, new JsonSerializerOptions()
            {
                AllowTrailingCommas = true,
                ReadCommentHandling = JsonCommentHandling.Skip,
                PropertyNameCaseInsensitive = true,
            }, cancellationToken);
            this.logger.LogTrace("Loaded configuration from {Path}", path);
            
            return RushConfiguration.FromRushJson(rushJson, path);
        }
    }
}