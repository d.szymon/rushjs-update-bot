﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RushJsUpdateBot.Interfaces;
using RushJsUpdateBot.Models;
using RushJsUpdateBot.Models.Configuration;

namespace RushJsUpdateBot.Services
{
    public class PackageUpdateService
    {
        private readonly IVersionControlService versionControlService;
        private readonly ILogger<PackageUpdateService> logger;
        private readonly VersionControlOptions versionControlOptions;
        private readonly RushJsService rushJsService;
        private readonly IVersionControlApiService versionControlApiService;

        public PackageUpdateService(IVersionControlService gitService,
            ILogger<PackageUpdateService> logger,
            IOptions<VersionControlOptions> versionControlOptions,
            RushJsService rushJsService,
            IVersionControlApiService versionControlApiService
        )
        {
            this.versionControlService = gitService;
            this.logger = logger;
            this.rushJsService = rushJsService;
            this.versionControlApiService = versionControlApiService;
            this.versionControlOptions = versionControlOptions.Value;
        }

        public async Task ProcessGroup(UpdateProject project, RushConfiguration rushConfiguration, PackageUpdateGroup group, CancellationToken cancellationToken)
        {
            this.logger.LogInformation("Processing group {GroupName} in {Path}", group.Name, rushConfiguration.RootDirectory);

            await ProcessMinorGroup(project, rushConfiguration, group, cancellationToken);
            await ProcessMajorGroup(project, rushConfiguration, group, cancellationToken);
        }

        protected async Task ProcessMinorGroup(UpdateProject project, RushConfiguration configuration, PackageUpdateGroup group, CancellationToken cancellationToken)
        {
            var updates = group.Updates.Where(u => u.HasMinorUpdate).ToList();

            if (!updates.Any())
            {
                this.logger.LogInformation("No minor updates for {GroupName}", group.Name);
                return;
            }

            var branchName = this.versionControlOptions.FormatMinorBranch(group.Name);
            await this.versionControlService.CreateBranchFromDefault(
                configuration.RootDirectory,
                branchName,
                cancellationToken
            );

            foreach (var update in updates)
            {
                await this.rushJsService.UpdatePackage(configuration, update.Package, update.Minor, cancellationToken);
                await this.rushJsService.AddOrUpdateChangelog(configuration, update.Package, update.Minor, cancellationToken);
            }

            await this.versionControlService.PushChanges(configuration.RootDirectory, $"{group.Name} update", cancellationToken);
            await this.versionControlApiService.CreateMergeRequest(project.Repository.Id.ToString(), branchName, cancellationToken);
            await this.rushJsService.Purge(configuration, cancellationToken);
        }

        protected async Task ProcessMajorGroup(UpdateProject project, RushConfiguration configuration, PackageUpdateGroup group, CancellationToken cancellationToken)
        {
            var updates = group.Updates.Where(u => u.HasMajorUpdate).ToList();

            if (!updates.Any())
            {
                this.logger.LogInformation("No major updates for {GroupName}", group.Name);
                return;
            }

            var branchName = this.versionControlOptions.FormatMajorBranch(group.Name); 
            await this.versionControlService.CreateBranchFromDefault(
                configuration.RootDirectory,
                branchName,
                cancellationToken
            );

            foreach (var update in updates)
            {
                await this.rushJsService.UpdatePackage(configuration, update.Package, update.Latest, cancellationToken);
                await this.rushJsService.AddOrUpdateChangelog(configuration, update.Package, update.Latest, cancellationToken);
            }

            await this.versionControlService.PushChanges(configuration.RootDirectory, $"{group.Name} major update", cancellationToken);
            await this.versionControlApiService.CreateMergeRequest(project.Repository.Id.ToString(), branchName, cancellationToken);
            await this.rushJsService.Purge(configuration, cancellationToken);
        }
    }
}