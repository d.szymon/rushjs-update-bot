﻿using System.Threading;
using System.Threading.Tasks;

namespace RushJsUpdateBot.Interfaces
{
    public interface IVersionControlService
    {
        Task ConfigureIdentity(string path, string name, string email, CancellationToken cancellationToken = default);
        Task CloneRepository(string project, CancellationToken cancellationToken = default);
        Task CreateBranchFromDefault(string path, string branchName, CancellationToken cancellationToken = default);
        Task PushChanges(string path, string message, CancellationToken cancellationToken = default);
    }
}