﻿using System.Threading;
using System.Threading.Tasks;

namespace RushJsUpdateBot.Interfaces
{
    public interface IVersionControlApiService
    {
        Task CreateMergeRequest(string projectId, string branch, CancellationToken cancellationToken = default);
    }
}