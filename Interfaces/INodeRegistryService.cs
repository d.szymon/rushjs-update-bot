﻿using System.Threading;
using System.Threading.Tasks;
using RushJsUpdateBot.Models;

namespace RushJsUpdateBot.Interfaces
{
    public interface INodeRegistryService
    {
        Task<NodeDependencyVersion> GetNewestMinor(NodeDependency package, CancellationToken cancellationToken = default);
        Task<NodeDependencyVersion> GetNewestVersion(NodeDependency package, CancellationToken cancellationToken = default);
    }
}