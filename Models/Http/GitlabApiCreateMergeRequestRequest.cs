﻿using System.Text.Json.Serialization;

namespace RushJsUpdateBot.Models.Http
{
    // https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
    public record GitlabApiCreateMergeRequestRequest
    {
        public string Id { get; init; }
        [JsonPropertyName("source_branch")]
        public string SourceBranch { get; init; }
        [JsonPropertyName("target_branch")]
        public string TargetBranch { get; init; }
        public string Title { get; init; }
        [JsonPropertyName("remove_source_branch")]
        public bool RemoveSourceBranch { get; init; } = true;
    }
}