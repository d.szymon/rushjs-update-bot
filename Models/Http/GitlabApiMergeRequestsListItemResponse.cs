﻿namespace RushJsUpdateBot.Models.Http
{
    public record GitlabApiMergeRequestsListItemResponse
    {
        public ulong Id { get; set; }
    }
}