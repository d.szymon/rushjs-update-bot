﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace RushJsUpdateBot.Models.Http
{
    public record NpmRegistryPackageResponse
    {
        public string Name { get; init; }
        public Dictionary<string, object> Versions { get; init; }
        [JsonPropertyName("dist-tags")]
        public Dictionary<string, string> DistTags { get; set; }
    }
}