﻿using System.Collections.Generic;
using System.Linq;

namespace RushJsUpdateBot.Models
{
    public partial class NodeDependency
    {
        protected static readonly string[] ExcludedVersions = new[]
        {
            "workspace",
            "npm",
            "git",
            "http",
        };
        public string Name { get; }
        public List<string> Projects { get; }
        public NodeDependencyVersion Version { get; }

        public NodeDependency(IEnumerable<string> projects, KeyValuePair<string, string> data)
        {
            this.Projects = projects.ToList();
            this.Name = data.Key;
            this.Version = new NodeDependencyVersion(data.Value);
        }

        public static bool IsSupported(string version)
        {
            return !version.Contains("-") && ExcludedVersions.All(v => !version.StartsWith(v));
        }
    }
}