﻿using System.Collections.Generic;

namespace RushJsUpdateBot.Models
{
    public class PackageJson
    {
        public string Name { get; init; }
        public Dictionary<string, string> Dependencies { get; init; }
    }
}