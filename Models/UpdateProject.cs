﻿using RushJsUpdateBot.Models.Configuration;

namespace RushJsUpdateBot.Models
{
    public class UpdateProject
    {
        public VersionControlOptions.RepositoryOptions Repository { get; }
        public string Path { get; }

        public UpdateProject(VersionControlOptions.RepositoryOptions repository, string path)
        {
            this.Repository = repository;
            this.Path = path;
        }
    }
}