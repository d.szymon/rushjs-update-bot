﻿using System.Collections.Generic;

namespace RushJsUpdateBot.Models.RushJs
{
    public class RushJson
    {
        public List<RushJsonProject> Projects { get; init; }
        
        public class RushJsonProject
        {
            public string PackageName { get; init; }
            public string ProjectFolder { get; init; }
        }
    }
}