﻿using System.Collections.Generic;

namespace RushJsUpdateBot.Models.RushJs
{
    public record RushChangelog(string PackageName, string Email, IEnumerable<RushChangelogChange> Changes);

    public record RushChangelogChange(string PackageName, string Comment, string Type = "none");
}
