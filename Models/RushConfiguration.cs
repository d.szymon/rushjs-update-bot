﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using RushJsUpdateBot.Models.RushJs;

namespace RushJsUpdateBot.Models
{
    public class RushConfiguration
    {
        public ReadOnlyCollection<RushProject> Projects { get; init; }
        public string ConfigurationPath { get; init; }

        public string RootDirectory => Path.GetDirectoryName(ConfigurationPath);
        public string ChangelogDirectory => Path.Join(RootDirectory, "common", "changes");

        public static RushConfiguration FromRushJson(RushJson json, string path)
        {
            return new RushConfiguration()
            {
                Projects = json.Projects.Select(p => RushProject.FromRushJsonProject(p, path)).ToList().AsReadOnly(),
                ConfigurationPath = path,
            };
        }

        public string GetProjectPath(string projectName)
        {
            var project = Projects.FirstOrDefault(p => p.Name.Equals(projectName));

            if (project == null)
            {
                throw new ArgumentException($@"Missing project ""{projectName}""", nameof(projectName));
            }

            return Path.Join(RootDirectory, project.Path);
        }

        public class RushProject
        {
            public string Name { get; init; }
            public string Path { get; init; }
            protected string RootPath { get; init; }

            public string PackageJsonPath => System.IO.Path.Join(System.IO.Path.GetDirectoryName(RootPath), Path, "package.json");

            public static RushProject FromRushJsonProject(RushJson.RushJsonProject project, string rootPath)
            {
                return new RushProject()
                {
                    Name = project.PackageName,
                    Path = project.ProjectFolder,
                    RootPath = rootPath,
                };
            }
        }
    }
}