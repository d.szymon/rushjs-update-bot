﻿namespace RushJsUpdateBot.Models
{
    public class PackageUpdate
    {
        public NodeDependency Package { get; }

        public NodeDependencyVersion Minor { get; }

        public NodeDependencyVersion Latest { get; }

        public bool HasMinorUpdate => !Minor.IsPrerelease && (Package.Version.Minor != Minor.Minor || Package.Version.Patch != Minor.Patch);
        public bool HasMajorUpdate => Latest.Major != Package.Version.Major && !Latest.IsPrerelease;

        public PackageUpdate(NodeDependency package, NodeDependencyVersion minor, NodeDependencyVersion latest)
        {
            Package = package;
            Minor = minor;
            Latest = latest;
        }
    }
}