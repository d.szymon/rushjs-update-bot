﻿using System.Collections.Generic;

namespace RushJsUpdateBot.Models.Configuration
{
    public class PackagesOptions
    {
        public List<PackageGroup> Groups { get; init; }
        public ushort SleepTime { get; init; }
        
        public class PackageGroup
        {
            public string Name { get; init; }
            public List<string> Packages { get; init; }
        }
    }
}