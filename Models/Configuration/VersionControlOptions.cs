﻿using System.Collections.Generic;
using System.IO;

namespace RushJsUpdateBot.Models.Configuration
{
    public class VersionControlOptions
    {
        public List<RepositoryOptions> Repositories { get; init; }
        public string CloneUrlTemplate { get; init; }
        public string WorkingDirectory { get; init; }
        public string DefaultBranch { get; init; }
        public string MinorUpdateBranch { get; init; }
        public string MajorUpdateBranch { get; init; }
        public VersionControlApiOptions Api { get; init; }
        public VersionControlIdentityOptions User { get; set; }

        public string FormatCloneUrl(string project)
        {
            return CloneUrlTemplate.Replace("{Project}", project);
        }

        public string FormatDirectory(string project)
        {
            return Path.Join(WorkingDirectory, project);
        }
        
        protected static string FormatBranch(string template, string groupName) => template
            .Replace("{GroupName}", groupName.Replace("@", ""));

        public string FormatMinorBranch(string groupName) => FormatBranch(MinorUpdateBranch, groupName);

        public string FormatMajorBranch(string groupName) => FormatBranch(MajorUpdateBranch, groupName);

        public record RepositoryOptions
        {
            public string Name { get; init; }
            public string Id { get; init; }
        }
        
        public class VersionControlApiOptions
        {
            public string AccessToken { get; init; }

            protected string NormalizedUrl;

            public string Url
            {
                get => NormalizedUrl;
                set => NormalizedUrl = value.TrimEnd('/');
            }
        }

        public class VersionControlIdentityOptions
        {
            public string Name { get; init; }
            public string Email { get; init; }
        }
    }
}