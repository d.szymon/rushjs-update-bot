﻿using System;

namespace RushJsUpdateBot.Models.Configuration
{
    public class NpmRegistryOptions
    {
        protected readonly string NormalizedApiUrl;

        public string ApiUrl
        {
            get => NormalizedApiUrl;
            init => NormalizedApiUrl = value.TrimEnd('/');
        }

        public string GetPackageUrl(string packageName)
        {
            return $"{ApiUrl}/{Uri.EscapeUriString(packageName)}";
        }
    }
}
