﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace RushJsUpdateBot.Models
{
    public class NodeDependencyVersion
    {
        protected static readonly Regex VersionRegex = new(@"(.*?)(\d.*?)\.(\d.*?)\.(\d.*?)(?:$|-)(.*?)$");
            
        public string Prefix { get; }
        public uint Major { get; }
        public uint Minor { get; }
        public uint Patch { get; }
        public string Suffix { get; }

        public bool IsPrerelease => !string.IsNullOrWhiteSpace(Suffix);

        public NodeDependencyVersion(string version)
        {
            var match = VersionRegex.Match(version, 0);

            if (!match.Success)
            {
                throw new ArgumentException($@"Invalid version ""{version}""", nameof(version));
            }

            Prefix = match.Groups[1].Value;
            Major = uint.Parse(match.Groups[2].Value);
            Minor = uint.Parse(match.Groups[3].Value);
            Patch = uint.Parse(match.Groups[4].Value);
            Suffix = string.IsNullOrWhiteSpace(match.Groups[5].Value) ? string.Empty : "-" + match.Groups[5].Value;
        }

        public override string ToString()
        {
            return $"{Prefix}{Major}.{Minor}.{Patch}{Suffix}";
        }

        public static NodeDependencyVersion GetNewestVersion(IEnumerable<NodeDependencyVersion> versions)
        {
            var filteredVersions = versions
                .Where(v => string.IsNullOrWhiteSpace(v.Suffix))
                .ToList();
            
            var highestMajor = filteredVersions.Max(v => v.Major);
            var highestMinor = filteredVersions
                .Where(v => v.Major == highestMajor)
                .Max(v => v.Minor);
            var highestPatch = filteredVersions
                .Where(v => v.Major == highestMajor && v.Minor == highestMinor)
                .Max(v => v.Patch);

            return filteredVersions.First(v =>
                v.Major == highestMajor && v.Minor == highestMinor && v.Patch == highestPatch);
        }
    }
}