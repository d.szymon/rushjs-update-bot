﻿using System.Collections.Generic;
using System.Linq;

namespace RushJsUpdateBot.Models
{
    public class PackageUpdateGroup
    {
        public string Name { get; }
        public List<PackageUpdate> Updates { get; }

        public PackageUpdateGroup(string name, IEnumerable<PackageUpdate> updates)
        {
            this.Name = name;
            this.Updates = updates.ToList();
        }

        public PackageUpdateGroup(string name, PackageUpdate update) : this(name, new[] {update})
        {

        }

        public PackageUpdateGroup(PackageUpdate update) : this(update.Package.Name, update)
        {
            
        }
    }
}